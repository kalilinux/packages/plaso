plaso (20240409-0kali1) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * d/watch: Update watch file for github tag page changes

  [ Sophie Brun ]
  * New upstream version 20240409
  * Refresh patches
  * Update installation
  * Update dependencies
  * Update lintian-overrides

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 28 Jun 2024 11:57:37 +0200

plaso (20211229-0kali4) kali-dev; urgency=medium

  * Remove future from requirements

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 29 Jan 2024 16:46:50 +0100

plaso (20211229-0kali3) kali-dev; urgency=medium

  * Remove obsolete deps on python3-six

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 23 Jan 2024 10:30:45 +0100

plaso (20211229-0kali2) kali-dev; urgency=medium

  * Remove obsolete dependency python3-future

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 22 Jan 2024 16:41:55 +0100

plaso (20211229-0kali1) kali-dev; urgency=medium

  * Team upload

  * Update debian/watch to follow updated Github link structure.
  * Import new upstream release 20211024. (Closes: #997833)
    + Refresh sole patch.
    + Update (build-)dependencies following changes in dependencies.ini.

  [ Sophie Brun ]
  * New upstream version 20211229
  * Update required version of build-deps and deps
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 19 May 2022 09:47:17 +0200

plaso (20201007-2) unstable; urgency=medium

  * Team upload

  * Refresh 0001-Add-vendored-bencode-2.0.patch to avoid offset warning.
  * Add a bunch of generated files in plaso.egg-info to debian/clean to be
    able to build twice in a row.
  * python3-plaso: Also install /usr/share/plaso/. (Closes: #976860)
  * Update 0001-Add-vendored-bencode-2.0.patch to also remove bencode
    dependency check from plaso/dependencies.py. Fixes "[FAILURE] missing:
    bencode." error.
  * Set "Rules-Requires-Root: no".
  * Bump debian/watch version from 3 to 4. Thanks Lintian!
  * Bump debhelper-compat to 13.
    + List LICENSE and README in debian/not-installed.
    + Install ACKNOWLEDGEMENTS and AUTHORS via d…/python3-plaso.install.
  * Remove one instance of duplicate (build-)deps on python3-dateutil.
  * Drop support for switching between python2 and python3 variants.
    + debian/rules: Drop override_dh_auto_install.
    + Drop debian/fix-scripts.sh.
    + Drop all maintainer scripts, no more needed.
    + debian/python3-plaso.install: Remove *-python3 wildcard.
  * Rename d…/lintian-overrides to d…/python3-plaso.lintian-overrides and
    update package name inside from plaso to python3-plaso.
    + Fixes lintian warning unused-override.
    + Overrides lintian warning script-with-language-extension again.
  * Replace lintian override for preg.py with psteal.py; update comment.
  * Add lintian overrides for
    package-contains-documentation-outside-usr-share-doc
    usr/share/plaso/*.txt as these are text-based data files, not
    documentation (and documentation about the directory contents).
  * debian/copyright: Update location of plaso/parsers/bencode.py to
    debian/patches/0001-Add-vendored-bencode-2.0.patch. Fixes lintian
    warnings wildcard-matches-nothing-in-dep5-copyright and
    unused-file-paragraph-in-dep5-copyright.
  * Declare 0001-Add-vendored-bencode-2.0.patch as debian-specific.
  * Declare compliance with Debian Policy 4.5.1. (No changes needed.)

 -- Axel Beckert <abe@debian.org>  Thu, 04 Feb 2021 23:57:32 +0100

plaso (20201007-1) unstable; urgency=medium

  * Team upload.

  [ Samuel Henrique ]
  * Configure git-buildpackage

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

  [ Raphaël Hertzog ]
  * New upstream version 20201007
  * Update Homepage

  [ Axel Beckert ]
  * Bump (build-)dependency on python3-dfvfs to a version no more using
    python3-crypto. Replace (indirect) (build-)dependency on
    python3-crypto with python3-cryptography. (Closes: #971308)
  * Bump versioned (build-)dependencies on libsigscan-python3,
    python3-libscca, -libregf, -libfsntfs, -libevt, -future,
    -elasticsearch, -dtfabric, -dfwinreg, -dfdatetime, and -artifacts
    according to upstream dependency declarations.
  * Follow upstream dependency declarations and
    + drop python3-efilter and -biplist from, and
    + add python3-cffi-backend, -dateutil, -defusedxml, -libcreg,
      -libfsext, -libluksde, and -redis to
    (build-)dependencies.

 -- Raphaël Hertzog <raphael@offensive-security.com>  Thu, 04 Feb 2021 17:59:51 +0100

plaso (20190131-3) unstable; urgency=medium

  * Team upload.
  * Drop python2 support; Closes: #937300
  * Bump pyparsind deps to >= 2.3.0; Closes: #923236

 -- Sandro Tosi <morph@debian.org>  Fri, 22 Nov 2019 17:30:31 -0500

plaso (20190131-2) unstable; urgency=medium

  * Adjust bencode import statement to work with Python3 import semantics
  * Add python2/python3 support

 -- Hilko Bengen <bengen@debian.org>  Mon, 11 Feb 2019 23:59:07 +0100

plaso (20190131-1) unstable; urgency=medium

  * New upstream version 20190131
  * Update bencode from bencode.py, remove python-bittorrent dependency

 -- Hilko Bengen <bengen@debian.org>  Sat, 09 Feb 2019 23:53:49 +0100

plaso (20181219-1) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org

  [ Hilko Bengen ]
  * New upstream version 20181219
  * Bump Debhelper compat level
  * Bump Standards-Version
  * Update dependencies, build-dependencies
  * Switch to pybuild
  * Drop patches
  * Add vendored bencode library

 -- Hilko Bengen <bengen@debian.org>  Wed, 16 Jan 2019 11:38:09 +0100

plaso (1.5.1+dfsg-4) unstable; urgency=medium

  * Fix bencode dependency checking
  * Support python-construct.legacy
  * Add python-artifacts dependency

 -- Hilko Bengen <bengen@debian.org>  Tue, 28 Nov 2017 01:52:17 +0100

plaso (1.5.1+dfsg-3) unstable; urgency=medium

  * Add python-efilter dependency (Closes: #847787)

 -- Hilko Bengen <bengen@debian.org>  Sat, 17 Dec 2016 00:23:52 +0100

plaso (1.5.1+dfsg-2) unstable; urgency=medium

  * Fix debian/control syntax error (Closes: #842048)

 -- Hilko Bengen <bengen@debian.org>  Mon, 14 Nov 2016 11:39:56 +0100

plaso (1.5.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.5.1+dfsg
  * Update dependencies

 -- Hilko Bengen <bengen@debian.org>  Mon, 24 Oct 2016 11:49:10 +0200

plaso (1.5.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.5.0+dfsg
  * Update watch to notice beta or rc releases
  * Update dependencies
  * Drop psutil compatibility patch integrated upstream

 -- Hilko Bengen <bengen@debian.org>  Wed, 21 Sep 2016 02:26:00 +0200

plaso (1.4.0+dfsg-2) unstable; urgency=medium

  * Fix dependencies
  * Add psutil version checks

 -- Hilko Bengen <bengen@debian.org>  Wed, 27 Jan 2016 22:58:08 +0100

plaso (1.4.0+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Hilko Bengen <bengen@debian.org>  Wed, 27 Jan 2016 08:47:52 +0100

plaso (1.3.0+dfsg-1) unstable; urgency=medium

  * Gave package to Debian Forensics team
  * Added dh-python to Build-Depends
  * Removed test data from orig tarball because it may contain non-free
    bits
  * Added script to automatically purge test_data/ from orig tarball;
    updated watch file
  * Added Lintian overrides for .py extensions

 -- Hilko Bengen <bengen@debian.org>  Wed, 05 Aug 2015 22:05:05 +0200

plaso (1.3.0-1) unstable; urgency=low

  * Initial release (Closes: #792335)

 -- Hilko Bengen <bengen@debian.org>  Thu, 23 Jul 2015 23:46:06 +0200
